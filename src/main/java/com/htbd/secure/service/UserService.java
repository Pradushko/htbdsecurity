package com.htbd.secure.service;

import com.htbd.secure.entity.User;

/**
 * Created by Admin on 17.10.16.
 */
public interface UserService {
    User getUser(String login);
}
