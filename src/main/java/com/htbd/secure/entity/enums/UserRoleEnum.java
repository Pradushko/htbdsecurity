package com.htbd.secure.entity.enums;

/**
 * Created by Admin on 17.10.16.
 */
public enum UserRoleEnum {
    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum() {
    }
}
